package dev.dersteffen.hookagent.generators;

import javassist.ClassPool;
import javassist.CtClass;

public interface IGenerator<T> {

	void generate(ClassPool pClassPool, T t, CtClass pCtClass) throws Exception;
	
	public static <T> IGenerator<T> empty() {
		return new IGenerator<T>() {
			@Override
			public void generate(ClassPool pClassPool, T t, CtClass pCtClass) throws Exception {}
		};
	}
	
}
