package dev.dersteffen.hookagent.generators.facade;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import org.objectweb.asm.Type;

import dev.dersteffen.hookagent.Logging;
import dev.dersteffen.hookagent.annotations.facade.ClassFacade;
import dev.dersteffen.hookagent.annotations.facade.MethodFacade;
import dev.dersteffen.hookagent.generators.IGenerator;
import dev.dersteffen.hookagent.mappings.old.MappingManagerOld;
import dev.dersteffen.hookagent.mappings.old.MethodMappingOld;
import dev.dersteffen.hookagent.mappings.old.MappingManagerOld.CheckType;
import dev.dersteffen.hookagent.utils.U;
import javassist.ClassPool;
import javassist.CtClass;
import javassist.CtMethod;
import javassist.CtNewMethod;
import javassist.NotFoundException;

public class FacadeMethodGenerator implements IGenerator<Method> {

	@Override
	public void generate(ClassPool pClassPool, Method pMethod, CtClass pCtClass) throws Exception {
		MethodFacade injectMethod = pMethod.<MethodFacade>getAnnotation(MethodFacade.class);
		String internalClassName = pMethod.getDeclaringClass().getAnnotation(ClassFacade.class).value();
		
		String methodName = injectMethod.value();
		String methodSignature = injectMethod.signature();
		
		MethodMappingOld methodMapping = MappingManagerOld.ME.getMappingByClassName(internalClassName, CheckType.UNOBF).getMethodMapping(methodName);
		if (methodMapping != null) {
			methodName = methodMapping.getObfName();
			methodSignature = methodMapping.getSignature();
		}
		
		boolean hasReturn = false;
		CtMethod getMethod = pCtClass.getMethod(methodName, methodSignature);
		if (!getMethod.getReturnType().getName().contains("void")) hasReturn = true;
		
		String returnStatement = hasReturn ? "return " : "";
		String methodString = U.createMethod(returnStatement + "$0." + methodName + "(" + this.getParameterPlaceholders(getMethod, pMethod.getParameterCount()) + ");");
						
		CtMethod method = CtNewMethod.make(
				Modifier.PUBLIC, // modifiers
				pClassPool.get(pMethod.getReturnType().getName().replace("/", ".")), // return-type
				pMethod.getName(), // name
				this.getNeededParameters(pClassPool, pMethod), // parameters
				new CtClass[] {}, // exceptions
				methodString, // body
				pCtClass // class destination
		);
		
		Logging.generateMethod(pMethod.getName(), methodName);
		pCtClass.addMethod(method);
	}
	
	private String getParameterPlaceholders(CtMethod targetMethod, int amount) throws Exception {
		
		StringBuilder placeholders = new StringBuilder();
		for (int i = 1; i <= amount; i++) {
			placeholders.append("(" + targetMethod.getParameterTypes()[i - 1].getName() + ")$" + i);
			if (i != amount) placeholders.append(',');
		}
		
		return placeholders.toString();
	}
	
	public CtClass[] getNeededParameters(ClassPool pClassPool, Method pMethod) {
		try {
			return pClassPool.get(pMethod.getDeclaringClass().getName()).getMethod(pMethod.getName(), Type.getMethodDescriptor(pMethod)).getParameterTypes();
		} catch (NotFoundException e) { e.printStackTrace(); }
		
		return null;
	}
	
}
