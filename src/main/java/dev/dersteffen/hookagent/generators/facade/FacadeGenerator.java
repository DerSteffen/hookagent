package dev.dersteffen.hookagent.generators.facade;

import dev.dersteffen.hookagent.generators.IGenerator;
import javassist.ClassPool;
import javassist.CtClass;

public class FacadeGenerator implements IGenerator<Class<?>> {

	@Override
	public void generate(ClassPool pClassPool, Class<?> pClass, CtClass pCtClass) throws Exception {
		String interfaceName = pClass.getName();
		CtClass interfaceClass = pClassPool.get(interfaceName.replace(".", "/"));
		try {
			pCtClass.addInterface(interfaceClass);
		} catch (RuntimeException ex) { ex.printStackTrace(); }
	}

}
  