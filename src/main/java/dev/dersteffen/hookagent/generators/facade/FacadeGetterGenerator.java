package dev.dersteffen.hookagent.generators.facade;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import dev.dersteffen.hookagent.Logging;
import dev.dersteffen.hookagent.annotations.facade.ClassFacade;
import dev.dersteffen.hookagent.annotations.facade.GetterFacade;
import dev.dersteffen.hookagent.generators.IGenerator;
import dev.dersteffen.hookagent.mappings.old.MappingManagerOld;
import dev.dersteffen.hookagent.mappings.old.MappingManagerOld.MappingType;
import dev.dersteffen.hookagent.utils.U;
import javassist.ClassPool;
import javassist.CtClass;
import javassist.CtMethod;
import javassist.CtNewMethod;

public class FacadeGetterGenerator implements IGenerator<Method> {
	
	@Override
	public void generate(ClassPool pClassPool, Method pMethod, CtClass pCtClass) throws Exception {
		GetterFacade injectGetter = pMethod.<GetterFacade>getAnnotation(GetterFacade.class);
		String fieldName = injectGetter.value();
		String internalClassName = pMethod.getDeclaringClass().getAnnotation(ClassFacade.class).value();
		fieldName = MappingManagerOld.ME.getObfName(internalClassName, fieldName, MappingType.FIELD);
		
		if (injectGetter.dontCreate()) {
			System.out.println("\t\tReflecting Getter for field '%obffield% <%field%>'"
		   			.replace("%field%", injectGetter.value())
		   			.replace("%obffield%", fieldName));
			return;
		}
			
		String methodString = U.createMethod("return $0." + fieldName + ";");
				
		Logging.generateGetter(injectGetter.value(), fieldName);
		CtMethod getterMethod = CtNewMethod.make(
				Modifier.PUBLIC, // modifiers
				pClassPool.get(pMethod.getReturnType().getName().replace("/", ".")), // return-type
				pMethod.getName(), // name
				new CtClass[] {}, // parameters
				new CtClass[] {}, // exceptions
				methodString, // body
				pCtClass // class destination
		);
		pCtClass.addMethod(getterMethod);
	}
	
}
