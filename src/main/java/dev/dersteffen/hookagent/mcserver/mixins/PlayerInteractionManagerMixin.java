package dev.dersteffen.hookagent.mcserver.mixins;

import dev.dersteffen.hookagent.annotations.mixin.ClassMixin;
import dev.dersteffen.hookagent.annotations.mixin.MethodMixin;
import dev.dersteffen.hookagent.annotations.mixin.ParamToField;
import dev.dersteffen.hookagent.annotations.mixin.ParamToParam;
import dev.dersteffen.hookagent.eventsystem.EventManager;
import dev.dersteffen.hookagent.eventsystem.events.RightClickBlockEvent;
import dev.dersteffen.hookagent.mcserver.facades.BlockPosition;
import dev.dersteffen.hookagent.mcserver.facades.EntityPlayer;
import dev.dersteffen.hookagent.mcserver.facades.World;

@ClassMixin("PlayerInteractionManager")
public class PlayerInteractionManagerMixin {

	@MethodMixin(
		value = "onRightClick",
		paramToField = {
			@ParamToField(index = 0, name = "entityPlayer")
		},
		paramToParam = {
			@ParamToParam(index = 1, targetIndex = 2),
			@ParamToParam(index = 2, targetIndex = 5)
		}
	)
	public void onRightClick(EntityPlayer pEntityPlayer, World pWorld, BlockPosition pBlockPosition) {
		EventManager.callEvent(new RightClickBlockEvent(pEntityPlayer, pWorld, pBlockPosition));
	}
	
}
