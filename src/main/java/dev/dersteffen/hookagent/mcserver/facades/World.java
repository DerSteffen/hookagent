package dev.dersteffen.hookagent.mcserver.facades;

import dev.dersteffen.hookagent.annotations.facade.ClassFacade;
import dev.dersteffen.hookagent.annotations.facade.MethodFacade;

@ClassFacade("World")
public interface World {
	
	@MethodFacade("getBlockState")
	BlockState getBlockState(BlockPosition pBlockPosition);
	
}
