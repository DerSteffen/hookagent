package dev.dersteffen.hookagent.mcserver.facades;

import dev.dersteffen.hookagent.annotations.facade.ClassFacade;
import dev.dersteffen.hookagent.annotations.facade.GetterFacade;

@ClassFacade("GameProfile")
public interface GameProfile {

	@GetterFacade(value = "name", dontCreate = true)
	String getName();
	
}
