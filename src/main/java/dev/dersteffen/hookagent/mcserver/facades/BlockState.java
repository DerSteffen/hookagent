package dev.dersteffen.hookagent.mcserver.facades;

import dev.dersteffen.hookagent.annotations.facade.ClassFacade;
import dev.dersteffen.hookagent.annotations.facade.MethodFacade;

@ClassFacade("BlockState")
public interface BlockState {

	@MethodFacade("getBlock")
	Block getBlock();
	
	@MethodFacade("test")
	boolean testMethod();
	
}
