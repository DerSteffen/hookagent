package dev.dersteffen.hookagent.mcserver.facades;

import dev.dersteffen.hookagent.annotations.facade.ClassFacade;

@ClassFacade("TileEntity")
public interface TileEntity {

}
