package dev.dersteffen.hookagent.mcserver.facades;

import dev.dersteffen.hookagent.annotations.facade.ClassFacade;
import dev.dersteffen.hookagent.annotations.facade.MethodFacade;

@ClassFacade("BlockPosition")
public interface BlockPosition {

	@MethodFacade("getX")
	int getX();
	
	@MethodFacade("getY")
	int getY();
	
	@MethodFacade("getZ")
	int getZ();
	
}
