package dev.dersteffen.hookagent.mappings;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import lombok.experimental.ExtensionMethod;

@ExtensionMethod(Arrays.class)
public enum MappingManager {
	ME;
	
	public enum MappingType { NORMAL, OBF }
	
	private List<ClassMapping> mappings = new ArrayList<>();

	public void load(Path pPath) throws Exception {
		if (!pPath.toFile().isDirectory()) throw new Exception("The file isn't a directory!");
		Files.list(pPath).filter(_path -> _path.endsWith(".json"))
						 .map(ClassMapping::createFromPath)
						 .filter(Optional::isPresent)
						 .map(Optional::get)
						 .forEach(mappings::add);
		
		
	}
	
	public Optional<ClassMapping> get(String pName, MappingType pMappingType) {
		switch(pMappingType) {
			case NORMAL: return this.getByName(pName);
			case OBF: return this.getByObfuscatedName(pName);
			default: return Optional.empty();
		}
	}
	
	private Optional<ClassMapping> getByName(String pName) {
		return this.mappings.stream().filter(_mapping -> _mapping.getName().equalsIgnoreCase(pName)).findFirst();
	}
	
	private Optional<ClassMapping> getByObfuscatedName(String pObfName) {
		return this.mappings.stream().filter(_mapping -> _mapping.getObfName().equalsIgnoreCase(pObfName)).findFirst();
	}
	
	public boolean isMapped(String pName, MappingType pMappingType) {
		return this.get(pName, pMappingType).isPresent();
	}
		
}
