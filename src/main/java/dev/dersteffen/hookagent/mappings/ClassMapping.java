package dev.dersteffen.hookagent.mappings;

import java.io.BufferedReader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import static dev.dersteffen.hookagent.utils.U.GSON;

@Getter
@AllArgsConstructor @NoArgsConstructor
public class ClassMapping {
	
	private String name;
	private String obfName;
			
	private final List<MethodMapping> methods = new ArrayList<MethodMapping>();
	private final Map<String, String> fields = new HashMap<String, String>();
	
	public Optional<MethodMapping> getMethodMapping(String pName) {
		return this.methods.stream().filter(mapping -> mapping.getName().equalsIgnoreCase(pName)).findFirst();
	}
	
	@Override
	public String toString() {
		return this.name + " | " + this.obfName;
	}
	
	public static Optional<ClassMapping> createFromPath(Path pPath) {
		try (BufferedReader reader = Files.newBufferedReader(pPath, StandardCharsets.UTF_8)) {
			return Optional.of(GSON.fromJson(reader, ClassMapping.class));
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		
		return Optional.empty();
	}
	
}
