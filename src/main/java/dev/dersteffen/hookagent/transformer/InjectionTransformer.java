  package dev.dersteffen.hookagent.transformer;

import java.lang.instrument.ClassFileTransformer;
import java.security.ProtectionDomain;

import dev.dersteffen.hookagent.InjectMain;
import dev.dersteffen.hookagent.mappings.MappingManager;
import dev.dersteffen.hookagent.mappings.MappingManager.MappingType;
import javassist.ClassPool;
import javassist.CtClass;

public class InjectionTransformer implements ClassFileTransformer {
	
	private final ClassPool _classPool;
	
	private final InjectionManager _injectionManager;
	private final MappingManager _mappingManager;
	
	public InjectionTransformer(InjectionManager injectionManager, MappingManager mappingManager) {
		this._classPool = ClassPool.getDefault();
		this._injectionManager = injectionManager;
		this._mappingManager = mappingManager;
	}

	@Override
	public byte[] transform(ClassLoader pLoader, String pClassName, Class<?> pClassBeingRedefined, ProtectionDomain pProtectionDomain, byte[] pClassfileBuffer) {
		pClassName = pClassName.replace("/", ".");
		
		if (!this._mappingManager.isMapped(pClassName, MappingType.OBF)) return pClassfileBuffer;
		try {
			CtClass ctClass = _classPool.get(pClassName);
			this._injectionManager.hook(_classPool, ctClass);
			ctClass.writeFile(InjectMain.JAR_FILE.getAbsolutePath() + "\\" + InjectMain.VERSION + "\\dumped");
			return ctClass.toBytecode();
		} catch (Exception e) { e.printStackTrace(); }
		
		return pClassfileBuffer;
	}

}
