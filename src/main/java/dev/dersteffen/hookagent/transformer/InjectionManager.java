package dev.dersteffen.hookagent.transformer;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import dev.dersteffen.hookagent.Logging;
import dev.dersteffen.hookagent.annotations.facade.ClassFacade;
import dev.dersteffen.hookagent.annotations.mixin.ClassMixin;
import dev.dersteffen.hookagent.factories.FactorySelector;
import dev.dersteffen.hookagent.factories.IGeneratorFactory;
import dev.dersteffen.hookagent.mappings.old.MappingManagerOld;
import dev.dersteffen.hookagent.mcserver.facades.Block;
import dev.dersteffen.hookagent.mcserver.facades.BlockPosition;
import dev.dersteffen.hookagent.mcserver.facades.BlockState;
import dev.dersteffen.hookagent.mcserver.facades.Entity;
import dev.dersteffen.hookagent.mcserver.facades.EntityLivingBase;
import dev.dersteffen.hookagent.mcserver.facades.EntityPlayer;
import dev.dersteffen.hookagent.mcserver.facades.EntityPlayerMP;
import dev.dersteffen.hookagent.mcserver.facades.GameProfile;
import dev.dersteffen.hookagent.mcserver.facades.World;
import dev.dersteffen.hookagent.mcserver.mixins.PlayerInteractionManagerMixin;
import javassist.ClassPool;
import javassist.CtClass;

public enum InjectionManager {
	ME;
	
	private List<Class<?>> facadeClasses = Arrays.asList(
		Entity.class,
		EntityLivingBase.class,
		EntityPlayer.class,
		EntityPlayerMP.class,
		GameProfile.class,
		World.class,
		Block.class,
		BlockState.class,
		BlockPosition.class
	);
	
	private List<Class<?>> mixinClasses = Arrays.asList(
		PlayerInteractionManagerMixin.class
	);
	
	public HashMap<String, List<Class<?>>> externalToInternal = new HashMap<String, List<Class<?>>>();
	
	private InjectionManager() {
		this.connectClasses(ClassFacade.class, this.facadeClasses);
		this.connectClasses(ClassMixin.class, this.mixinClasses);
	}
	
	private void connectClasses(Class<? extends Annotation> pAnnotation, List<Class<?>> pClasses) {
		try {
			for (Class<?> facadeClass : pClasses) {
				Annotation classFacade = facadeClass.getAnnotation(pAnnotation);
				String invalidClass = "_INVALID_CLASS_" + facadeClass.getName();
				String externalName = invalidClass;
				if (classFacade != null) externalName = MappingManagerOld.ME.getObfName((String)classFacade.getClass().getMethod("value").invoke(classFacade));
				if (externalName.equalsIgnoreCase("")) externalName = invalidClass;
				
				List<Class<?>> internalClasses = this.externalToInternal.get(externalName);
				if (internalClasses == null) {
					internalClasses = new ArrayList<Class<?>>();
					this.externalToInternal.put(externalName, internalClasses);
				}
				
				internalClasses.add(facadeClass);
			}
		} catch (Exception e) { e.printStackTrace(); }
	}
	
	public void hook(ClassPool pClassPool, CtClass pCtClass) throws Exception {
		List<Class<?>> internalClasses = this.externalToInternal.get(pCtClass.getName());
		if (internalClasses != null) {
			for (Class<?> internalClass : internalClasses) {
				Logging.modifyClass(internalClass.getSimpleName(), pCtClass.getName());

				IGeneratorFactory generatorFactory = FactorySelector.select(internalClass);
				generatorFactory.select(internalClass).generate(pClassPool, internalClass, pCtClass);				
				
				for (Method method : internalClass.getDeclaredMethods())
					generatorFactory.select(method).generate(pClassPool, method, pCtClass);
			}
		}
	}
	
}


