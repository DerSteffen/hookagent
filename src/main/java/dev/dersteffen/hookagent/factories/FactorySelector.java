package dev.dersteffen.hookagent.factories;

import dev.dersteffen.hookagent.annotations.facade.ClassFacade;
import dev.dersteffen.hookagent.annotations.mixin.ClassMixin;
import dev.dersteffen.hookagent.factories.impl.FacadeGeneratorFactory;
import dev.dersteffen.hookagent.factories.impl.MixinGeneratorFactory;

public class FactorySelector {
	
	private static final FacadeGeneratorFactory FACADE_FACTORY = new FacadeGeneratorFactory();
	private static final MixinGeneratorFactory MIXIN_FACTORY = new MixinGeneratorFactory();
	
	public static IGeneratorFactory select(Class<?> pClass) {
		if (pClass.getAnnotation(ClassFacade.class) != null) {
			return FACADE_FACTORY;
		} else if (pClass.getAnnotation(ClassMixin.class) != null) {
			return MIXIN_FACTORY;
		}
		
		return IGeneratorFactory.empty();
	}
	
}
