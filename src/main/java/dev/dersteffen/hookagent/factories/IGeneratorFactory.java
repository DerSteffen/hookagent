package dev.dersteffen.hookagent.factories;

import java.lang.reflect.Method;

import dev.dersteffen.hookagent.generators.IGenerator;

public interface IGeneratorFactory {

	IGenerator<Method> select(Method pMethod);
	IGenerator<Class<?>> select(Class<?> pClass);
	
	public static IGeneratorFactory empty() {
		return new IGeneratorFactory() {
			@Override
			public IGenerator<Method> select(Method pMethod) { return IGenerator.<Method>empty(); }

			@Override
			public IGenerator<Class<?>> select(Class<?> pClass) { return IGenerator.<Class<?>>empty(); }
		};
	}
	
}
